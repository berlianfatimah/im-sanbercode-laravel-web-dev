<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    
    public function register(){
        return view('form');
    }

    // public function welcome(){
    //     return view('welcome');
    // }

    public function kirim(Request $request){
        $namaDepan= $request['firstname'];
        $namaBelakang= $request['lastname'];

        return view('welcome',['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
