<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label for="">First name:</label> <br>
        <input type="text" name="firstname"> <br>
        <label for="">Last name:</label> <br>
        <input type="text" name="lastname"> <br>
        <label for="">Gender</label> <br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender" id="">other <br>
        <label for="">Nationality:</label> <br>
        <select name="nationality" id="">
            <option value="id">Indonesia</option>
            <option value="en">English</option> 
        </select> <br>
        <label for="">Language Spoken:</label> <br>
        <input type="checkbox" name="language" id="">Bahasa Indonesia <br>
        <input type="checkbox" name="language" id="">English <br>
        <input type="checkbox" name="language" id="">Other <br>
        <label for="">Bio</label> <br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="kirim">
    </form>
    
</body>
</html>