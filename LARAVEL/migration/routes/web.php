<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

//create data
Route::get('/cast/create', [CastController::class, 'create']);
//menyimpan data ke DB
Route::post('/cast', [CastController::class, 'store']);
//ke halaman tampil semua data di tabel cast
Route::get('/cast',[CastController::class, 'index']);
//ke halaman detail
Route::get('/cast/{cast_id}',[CastController::class, 'show']) ;

//edit data
//edit data mengarah ke form
Route::get('/cast/{cast_id}/edit',[CastController::class, 'edit']);
//edit data berdasarkan id
Route::put('/cast/{cast_id}',[CastController::class, 'update']);

//delete
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
