@extends('master')

@section('title')
    Data Cast
@endsection

@section('sub-title')
    Cast
@endsection


@section('content')
<h1>{{$cast->nama}}</h1>
<h4 class="card-subtitle mb-2 text-muted">{{$cast->umur}}</h4>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn-sm btn-secondary">kembali</a>

@endsection