@extends('master')

@section('title')
    Data Cast
@endsection

@section('sub-title')
    Cast
@endsection


@section('content')
<a href="/cast/create" class="btn btn-primary">Add Cast</a>
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Detail</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($cast as $key => $item)
        <tr>
        <th scope="row">{{$key+1}}</th>
        <td>{{$item->nama}}</td>
        <td>
            
            <form action="/cast/{{$item->id}}" method="post">
              @csrf
              @method('delete')
              <a href="/cast/{{$item->id}}" class="btn btn-sm btn-success">Detail</a>
              <a href="/cast/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>

              <input type="submit" value="Delete" class="btn btn-sm btn-danger">

            </form>
        </td>
        </tr>
    @endforeach
  </tbody>
</table>

@endsection