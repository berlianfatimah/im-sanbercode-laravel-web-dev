@extends('master')

@section('title')
    Tambah Data Cast
@endsection

@section('sub-title')
    Cast
@endsection


@section('content')
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('put')
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" name="nama" value="{{$cast->nama}}">
            @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Umur</label>
            <input type="text" class="form-control" name="umur" value="{{$cast->umur}}">
            @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Bio</label>
            <textarea class="form-control" rows="3" name="bio">{{$cast->bio}}</textarea>
            @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <!-- <input type="text" class="form-control" > -->
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
@endsection